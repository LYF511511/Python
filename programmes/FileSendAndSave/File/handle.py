import web
import os
import time

class FileSendHandle(object):
    def GET(self):
        os.chdir('E:/Python/html/File/html')
        return open('E:/Python/html/File/html/Send.html', 'r', encoding='utf-8').read()
    
    def POST(self):
        Dict = web.input(file={})
        if 'file' in Dict:
            try:
                os.chdir('E:/Python/html/File')
                open('files/'+ Dict['file'].filename,'wb').write(Dict['file'].file.read())
            except:
                return open('E:/Python/html/File/html/Error.html', 'r', encoding='utf-8').read()
        return open('E:/Python/html/File/html/Ok.html', 'r', encoding='utf-8').read()


class FileSaveHandle(object):
    def GET(self):
        os.chdir('E:/Python/html/File/html')
        return open('E:/Python/html/File/html/Save.html', 'r', encoding='utf-8').read()


class FileSendAndSaveHandle(object):
    def GET(self):
        os.chdir('E:/Python/html/File/html')
        return open('E:/Python/html/File/html/SendAndSave.html', 'r', encoding='utf-8').read()


class FileGetFileHandle(object):
    def GET(self, file):
        os.chdir('E:/Python/html/File')
        return open(file, 'rb').read()


class FileGetFileListHandle(object):
    def GET(self):
        os.chdir('E:/Python/html/File/files')
        ret = '<!DOCTYPE html><html><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><body>'''
        for each in os.listdir():
            ret += '<a href="file/'+each+'" download="'+each+'">文件名：'+each+'  大小：'+str(os.path.getsize(each))+'  上传时间：'+time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(os.path.getctime(each)))+'</a><br>'
        ret += '</body></html>'
        return ret


class FileDLFile(object):
    def GET(self, url):
        os.chdir('E:/Python/html/File/files')
        return open(url, 'rb').read()
