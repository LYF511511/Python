import web
from sys import path
path.append('File\\')
from File.handle import FileSendHandle, FileSaveHandle, FileSendAndSaveHandle, FileGetFileHandle, FileGetFileListHandle, FileDLFile
from web.template import ALLOWED_AST_NODES

ALLOWED_AST_NODES.append('Constant')
urls = (
    '/File/Send', 'FileSendHandle',
    '/File/Save', 'FileSaveHandle',
    '/File/SendAndSave', 'FileSendAndSaveHandle',
    '/File/(static/.+\..+)', 'FileGetFileHandle',
    '/File/GetFileList', 'FileGetFileListHandle',
    '/File/file/(.+\..+)', 'FileDLFile',
)


app = web.application(urls, globals())
app.run()
